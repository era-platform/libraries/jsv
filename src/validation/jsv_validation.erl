%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 16.10.2021
%%% @doc Implementation of the json compliance check for the passed description.

-module(jsv_validation).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([check/2]).

%% for check functions in V3DU
-export([add_path/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("jsv.hrl").

-include_lib("eunit/include/eunit.hrl").

-define(MLogPrefix, ?MODULE).

%% debug out
%%-define(VOUT(Text,Args), ?OUT(Text,Args)).
%%-define(VOUT(Text), ?OUT(Text)).
-define(VOUT(Text,Args), ok).
-define(VOUT(Text), ok).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec check(Json::json(), Descriptor::map()) -> {ok, Result::map()} | {error, Reason::map()}.
%% ---
check(Json,Descriptor) ->
    ?LOG('$trace',"~p. <-- Start check json: ~n~120p~nby dsc:~n~120p",[?MLogPrefix,Json,Descriptor]),
    StartKey = <<"/">>,
    CheckState = #check_s{key = StartKey,
                          value = Json,
                          dsc = Descriptor,
                          path = StartKey, % init path==root (/)
                          json = Json,
                          callbacks = #check_cb{check_data = fun check_data/1,
                                                exec_checkfun = fun exec_checkfun/2,
                                                add_path = fun add_path/2,
                                                clear_state = fun clear_state/1
                                               }
                         },
    Result = check_data(CheckState),
    ?LOG('$trace',"~p. --> Check json complete with result: ~n~120p",[?MLogPrefix,Result]),
    Result.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
check_data(CS) ->
    #check_s{key=Key,dsc=Descriptor}=CS,
    Type = maps:get(<<"type">>,Descriptor),
    check_type(CS#check_s{key=?BU:to_binary(Key),type=?BU:to_binary(Type)}).

%% @private
check_type(CS) ->
    #check_s{type=T,key=K}=CS,
    case ets:lookup(?TypesEts,T) of
        [{T,TypeModule}] -> do_check_type(CS, TypeModule);
        [] ->
            Reason = ?UErrMsg:make_reason_by_text("unknown type ~s at key '~s'",[T,K],CS),
            make_result({error,Reason})
    end.

%% ---
do_check_type(CS, TypeModule) ->
    #check_s{type=Type,key=Key}=CS,
    CS1 = CS#check_s{types_state=undefined},
    case catch TypeModule:check_data_by_type(CS1) of
        {'EXIT', ExecErrReason} ->
            ?LOG('$trace',"~p. do_check_type. exec type check_data crash: ~120p",[?MLogPrefix,ExecErrReason]),
            Reason = ?UErrMsg:make_reason_by_text("unknown type '~s' or check crash. For key '~s'",[Type,Key],CS1),
            make_result({error,Reason});
        Result -> make_result(Result)
    end.

%% @private
make_result(ok) ->
    {ok, #{}};
make_result({ok, Warnings}) when is_list(Warnings) ->
    {ok, #{warnings => Warnings}};
make_result({error, Errors}) when is_list(Errors) ->
    {error, #{errors => Errors}};
make_result({error, Errors, Warnings}) when is_list(Errors) andalso is_list(Warnings) ->
    Result = #{errors => Errors,
               warnings => Warnings},
    {error, Result}.

%% -----------
%% Internal callbacks
%% -----------

%% ---
exec_checkfun([M,F,MapArgs],CS) ->
    #check_s{key=K,value=V,json=Json,path=P}=CS,
    Args = MapArgs#{json => Json,
                    path => P},
    case M:F(K,V,Args) of
        ok -> ok;
        {ok,_Warnings}=Okx -> Okx;
        {error,Errors}=Err when is_map(Errors) -> Err;
        {error,Errors,Warnings}=Errx when is_map(Errors) andalso is_map(Warnings) -> Errx;
        _ ->
            Reason = ?UErrMsg:make_reason_by_text("invalid return type for checkfun. ~s:~s",[M,F],CS),
            {error, Reason}
    end.

%% ---
%% need clear state (error and warnings) before recursive call
clear_state(CS) ->
    CS#check_s{path_lidx=0,errors=[],warnings=[]}.

%% ---
add_path(Key,<<"/">> = Path) when (is_binary(Key) orelse is_integer(Key)) andalso is_binary(Path) ->
    <<Path/binary,(?BU:to_binary(Key))/binary>>;
add_path(Key,Path) when (is_binary(Key) orelse is_integer(Key)) andalso is_binary(Path) ->
    <<Path/binary,"/",(?BU:to_binary(Key))/binary>>.
