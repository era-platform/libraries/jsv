%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 16.10.2021 20:53
%%% @doc

-module(jsv_validation_types_array).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([type_name/0,
         check_data_by_type/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("jsv.hrl").

-define(TypeName, <<"array">>).
-define(MLogPrefix, ?MODULE).

%% debug out
%%-define(VOUT(Text,Args), ?OUT(Text,Args)).
%%-define(VOUT(Text), ?OUT(Text)).
-define(VOUT(Text,Args), ok).
-define(VOUT(Text), ok).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec type_name() -> TypeName::binary().
%% ---
type_name() -> ?TypeName.

%% ---
-spec check_data_by_type(CheckState::#check_s{}) -> ok | {ok, Warnings::[map()]} | {error, Errors::[map()]} | {error, Errors::[map()], Warnings::[map()]}.
%% ---
check_data_by_type(#check_s{type=?TypeName,value=V}=CS) when is_list(V) ->
    #check_s{dsc=Descriptor,callbacks=#check_cb{exec_checkfun=ExecChFun}}=CS,
    [ListEl,CheckF,LValues] = ?BU:maps_get_default([{<<"array_element">>,undef},
                                                    {<<"checkfun">>,undef},
                                                    {<<"values">>,undef}],Descriptor),
    if
        ListEl/=undef -> check_data_array(CS#check_s{key=undefined,dsc=ListEl});
        CheckF/=undef -> ExecChFun(CheckF,CS);
        LValues/=undef -> check_array_values(LValues,CS);
        true -> ok
    end;
check_data_by_type(CS) ->
    #check_s{key=K,type=T}=CS,
    ?VOUT("check_data_by_type. Invalid value for type '~s' CS:~120p",[?TypeName,CS]),
    Reason = ?UErrMsg:make_reason_by_text("invalid '~s'. Value must be '~s'",[K,T],CS),
    {error,Reason}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
check_data_array(#check_s{value=[],errors=[],warnings=[]}) -> ok;
check_data_array(#check_s{value=[],errors=[],warnings=W}) -> {ok,W};
check_data_array(#check_s{value=[],errors=E,warnings=[]}) -> {error,E};
check_data_array(#check_s{value=[],errors=E,warnings=W}) -> {error,E,W};
%check_data_array(#check_s{errors=E,warnings=[]}) when E/=[] -> {error,E};
%check_data_array(#check_s{errors=E,warnings=W}) when E/=[] -> {error,E,W};
check_data_array(#check_s{value=[Vx|T]}=CS) ->
    #check_s{callbacks=CBs,path=Path,path_lidx=Pidx,errors=E,warnings=W}=CS,
    #check_cb{check_data=CheckDataFun,add_path=AddPathFun,clear_state=ClearSFun}=CBs,
    CS1 =
        case CheckDataFun(ClearSFun(CS#check_s{value=Vx,path=AddPathFun(Pidx,Path)})) of
            {ok, ResMap} ->
                Warns1 = maps:get(warnings,ResMap,[]),
                CS#check_s{warnings=Warns1++W};
            {error, ErrMap} ->
                Errors1 = maps:get(errors,ErrMap),
                Warns1 = maps:get(warnings,ErrMap,[]),
                CS#check_s{errors=Errors1++E, warnings=Warns1++W}
        end,
    check_data_array(CS1#check_s{value=T,path_lidx=Pidx+1}).

%% ---
check_array_values(LValues,CS) ->
    #check_s{key=K,value=CheckValues}=CS,
    case CheckValues -- LValues of
        [] -> ok;
        [_|_]=ErrList ->
            ?LOG('$trace',"~p. <-- check_array_values. Error list: ~120p",[?MLogPrefix,ErrList]),
            AllowedValues = <<"'",(?BU:join_binary(LValues,<<"', '">>))/binary,"'">>,
            Reason = ?UErrMsg:make_reason_by_text("invalid '~s'. Value must be one of ~s and not repeated",[K,AllowedValues],CS),
            {error,Reason}
    end.