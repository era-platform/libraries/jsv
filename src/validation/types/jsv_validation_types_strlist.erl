%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 17.10.2021 21:33
%%% @doc

-module(jsv_validation_types_strlist).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([type_name/0,
         check_data_by_type/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("jsv.hrl").

-define(TypeName, <<"strlist">>).
-define(MLogPrefix, ?MODULE).

%% debug out
%%-define(VOUT(Text,Args), ?OUT(Text,Args)).
%%-define(VOUT(Text), ?OUT(Text)).
-define(VOUT(Text,Args), ok).
-define(VOUT(Text), ok).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec type_name() -> TypeName::binary().
%% ---
type_name() -> ?TypeName.

%% ---
-spec check_data_by_type(CheckState::#check_s{}) -> ok | {ok, Warnings::[map()]} | {error, Errors::[map()]} | {error, Errors::[map()], Warnings::[map()]}.
%% ---
check_data_by_type(#check_s{type=?TypeName,value=V}=CS) when is_list(V) ->
    #check_s{key=K,dsc=Descriptor,callbacks=CBs}=CS,
    #check_cb{exec_checkfun=ExecCheckFun}=CBs,
    %
    [CheckF] = ?BU:maps_get_default([{<<"checkfun">>,undef}],Descriptor),
    %
    AllowedStrKeys = [<<"checkfun">>,<<"type">>],
    DescrX = maps:without(AllowedStrKeys,Descriptor),
    if
        map_size(DescrX)/=0 ->
            Reason = ?UErrMsg:make_reason_by_text("Invalid str metadata for key '~s'.",[K],CS),
            {error,Reason};
        CheckF/=undef -> ExecCheckFun(CheckF,CS);
        true ->
            case lists:all(fun(Vx) -> is_binary(Vx) end,V) of
                true -> ok;
                false ->
                    Reason = ?UErrMsg:make_reason_by_text("invalid '~s'. All values must be string",[K],CS),
                    {error,Reason}
            end
    end;
check_data_by_type(CS) ->
    #check_s{key=K,type=T}=CS,
    ?VOUT("check_data_by_type. Invalid value for type '~s' CS:~120p",[?TypeName,CS]),
    Reason = ?UErrMsg:make_reason_by_text("invalid '~s'. Value must be '~s'",[K,T],CS),
    {error,Reason}.

%% ====================================================================
%% Internal functions
%% ====================================================================