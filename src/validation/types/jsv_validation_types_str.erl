%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 17.10.2021 21:29
%%% @doc

-module(jsv_validation_types_str).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([type_name/0,
         check_data_by_type/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("jsv.hrl").

-define(TypeName, <<"str">>).
-define(MLogPrefix, ?MODULE).

%% debug out
%%-define(VOUT(Text,Args), ?OUT(Text,Args)).
%%-define(VOUT(Text), ?OUT(Text)).
-define(VOUT(Text,Args), ok).
-define(VOUT(Text), ok).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec type_name() -> TypeName::binary().
%% ---
type_name() -> ?TypeName.

%% ---
-spec check_data_by_type(CheckState::#check_s{}) -> ok | {ok, Warnings::[map()]} | {error, Errors::[map()]} | {error, Errors::[map()], Warnings::[map()]}.
%% ---
check_data_by_type(#check_s{type=?TypeName,value=V}=CS) when is_binary(V) ->
    #check_s{key=K,dsc=Descriptor,callbacks=CBs}=CS,
    #check_cb{exec_checkfun=ExecCheckFun}=CBs,
    %
    [CheckF,StrValues,XsvRule] = ?BU:maps_get_default([{<<"checkfun">>,undef},{<<"values">>,undef},{<<"xsv">>,undef}],Descriptor),
    %
    AllowedStrKeys = [<<"checkfun">>,<<"type">>,<<"values">>,<<"xsv">>],
    DescrX = maps:without(AllowedStrKeys,Descriptor),
    if
        map_size(DescrX)/=0 ->
            Reason = ?UErrMsg:make_reason_by_text("Invalid str metadata for key '~s'.",[K],CS),
            {error,Reason};
        CheckF/=undef -> ExecCheckFun(CheckF,CS);
        StrValues/=undef -> check_str_value(CS#check_s{dsc=StrValues});
        XsvRule/=undef -> check_str_xsv_value(CS#check_s{dsc=XsvRule});
        true -> ok % for str without check funs. clear str
    end;
check_data_by_type(CS) ->
    #check_s{key=K,type=T}=CS,
    ?VOUT("check_data_by_type. Invalid value for type '~s' CS:~120p",[?TypeName,CS]),
    Reason = ?UErrMsg:make_reason_by_text("invalid '~s'. Value must be '~s'",[K,T],CS),
    {error,Reason}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
check_str_value(#check_s{dsc=AllowedValues}=CS) when is_list(AllowedValues) ->
    #check_s{key=K,value=V,dsc=AllowedValues}=CS,
    case lists:member(V,AllowedValues) of
        true -> ok;
        false ->
            {error,?UErrMsg:make_reason_by_text("Invalid '~s' value.",[K],CS)}
    end;
check_str_value(#check_s{key=K}=CS) ->
    {error,?UErrMsg:make_reason_by_text("Invalid str metadata for key '~s'. 'values' must be list",[K],CS)}.

%% ---
check_str_xsv_value(#check_s{dsc=XsvRule}=CS)
  when is_map_key(<<"separator">>, XsvRule) andalso is_map_key(<<"values">>, XsvRule) ->
    #check_s{key=K,value=V}=CS,
    [Separator,Values] = ?BU:maps_get([<<"separator">>,<<"values">>],XsvRule),
    Vals = binary:split(V, Separator, [global,trim_all]),
    ?VOUT("check_str_xsv_value. V:~120p",[V]),
    ?VOUT("check_str_xsv_value. Vals:~120p",[Vals]),
    ?VOUT("check_str_xsv_value. Values:~120p",[Values]),
    case Vals -- Values of
        [] -> ok;
        [_|_] ->
            {error,?UErrMsg:make_reason_by_text("Invalid '~s' value.",[K],CS)}
    end;
check_str_xsv_value(#check_s{key=K}=CS) ->
    {error,?UErrMsg:make_reason_by_text("Invalid str metadata for key '~s'. 'xsv' must contain 'separator' and 'values' keys",[K],CS)}.