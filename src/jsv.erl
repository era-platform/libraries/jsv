%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 15.10.2021 20:21

-module(jsv).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([start/0, start/2,
         stop/0, stop/1,
         %
         check/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("jsv.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------
%% Application
%% ------------------

%% ---
-spec start() -> ok | {error,Reason::term()}.
%% ---
start() ->
    add_deps_paths(),
    setup_deps_env(),
    Result = application:ensure_all_started(?APP, permanent),
    ?OUTL("Application start. Result:~120p",[Result]),
    case Result of
        {ok, _Started} -> ok;
        {error,_}=Error -> Error
    end.

%% ---
-spec stop() -> ok | {error,Reason::term()}.
%% ---
stop() ->
    application:stop(?APP).

%% ---
start(_Mode, State) ->
    ?OUTL("Application start(Mode, State)", []),
    ?SUPV:start_link(State).

%% ---
stop(_State) ->
    ok.

%% ---
-spec check(Json::json(), Descriptor::map()) -> {ok,Result::map()} | {error,Reason::map()}.
%% ---
check(Json,Descriptor) ->
    ?JV:check(Json,Descriptor).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
%% adds app's dependencies paths to code
add_deps_paths() ->
    Path = ?U:drop_last_subdir(code:which(?MODULE), 3),
    DepsPaths = filelib:wildcard(Path++"/*/ebin"),
    AddPathsRes = code:add_pathsa(DepsPaths),
    AddPathsRes.

%% ---
setup_deps_env() ->
    ?BU:set_env(?APPBL,max_loglevel,'$info').