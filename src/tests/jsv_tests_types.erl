%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 20.10.2021 18:46
%%% @doc

-module(jsv_tests_types).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-ifdef(TEST).

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("jsv.hrl").
-include_lib("eunit/include/eunit.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------
%% Setup tests
%% -----------

%% ---
include_environment_test_() ->
    {ok,CWD} = file:get_cwd(),
    Path = filename:join([CWD, "_build", "default", "lib", "basiclib", "ebin"]),
    AddPathRes = code:add_patha(Path),
    {"include_environment_test_ test", [?_assertEqual(true, AddPathRes)]}.

%% ---
setup_test_() ->
    ?SUPV:init_validation_types_ets(),
    ?SUPV:fill_validation_types_ets(),
    [].

%% -----------
%% Data type tests
%% -----------

%% bool
type_bool_test_() ->
    Descriptor = dsc_bool(0),
    DescriptorCheckFun = dsc_bool(1),
    Json1 = json_bool(0),
    Json2 = json_bool(1),
    {"type_bool_test_",
     [
      ?_assertMatch({ok,_}, ?JV:check(Json1,Descriptor)),
      ?_assertMatch({ok,_}, ?JV:check(Json2,Descriptor)),
      ?_assertMatch({error,_}, ?JV:check(Json1,DescriptorCheckFun))
     ]}.

%% floatint
type_floatint_test_() ->
    DescriptorUnlim = dsc_floatint(0),
    DescriptorLim = dsc_floatint(1),
    Json1 = json_floatint(-10),
    Json2 = json_floatint(1),
    Json3 = json_floatint(-10.0),
    Json4 = json_floatint(1.0),
    {"type_floatint_test_",
     [
      ?_assertMatch({ok,_}, ?JV:check(Json1,DescriptorUnlim)),
      ?_assertMatch({ok,_}, ?JV:check(Json2,DescriptorLim)),
      ?_assertMatch({error,_}, ?JV:check(Json1,DescriptorLim)),
      ?_assertMatch({ok,_}, ?JV:check(Json3,DescriptorUnlim)),
      ?_assertMatch({ok,_}, ?JV:check(Json4,DescriptorLim)),
      ?_assertMatch({error,_}, ?JV:check(Json3,DescriptorLim))
     ]}.

%% int
type_int_test_() ->
    DescriptorUnlim = dsc_int(0),
    DescriptorLim = dsc_int(1),
    Json1 = json_int(-10),
    Json2 = json_int(1),
    {"type_int_test_",
     [
      ?_assertMatch({ok,_}, ?JV:check(Json1,DescriptorUnlim)),
      ?_assertMatch({ok,_}, ?JV:check(Json2,DescriptorLim)),
      ?_assertMatch({error,_}, ?JV:check(Json1,DescriptorLim))
     ]}.

%% str
type_str_test_() ->
    Descriptor = dsc_str(10),
    DescriptorCheckFun = dsc_str(11),
    DescriptorValues = dsc_str(20),
    DescriptorXsv = dsc_str(30),
    Json1 = json_str(10),
    Json2 = json_str(20),
    Json3 = json_str(30),
    {"type_str_test_",
     [
      ?_assertMatch({ok,_}, ?JV:check(Json1,Descriptor)),
      ?_assertMatch({error,_}, ?JV:check(Json1,DescriptorCheckFun)),
      ?_assertMatch({ok,_}, ?JV:check(Json2,DescriptorValues)),
      ?_assertMatch({ok,_}, ?JV:check(Json3,DescriptorXsv))
     ]}.

%% object
type_object_test_() ->
    Descriptor = dsc_obj(10),
    Json1 = json_obj(10),
    ?debugFmt("ets:~120p~n",[ets:tab2list(?TypesEts)]),
    {"type_obj_test_",
     [
      ?_assertMatch({ok,_}, ?JV:check(Json1,Descriptor))
     ]}.

%% -----------
%% Cleanup tests
%% -----------

%% ---
cleanup_test_() ->
    ets:delete(?TypesEts),
    [].


%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------
%% Data types examples
%% -----------
%% decoded by jsx json

%% ---
%% bool data
json_bool(0) -> false;
json_bool(1) -> true.
%% bool descriptor
dsc_bool(0) -> #{<<"type">> => bool};
dsc_bool(1) -> #{<<"type">> => bool, <<"checkfun">> => [erlang,make_ref,[]]}.

%% ---
%% floatint data
json_floatint(I) -> I.
%% floatint descriptor
dsc_floatint(0) -> #{<<"type">> => floatint};
dsc_floatint(1) -> #{<<"type">> => floatint, <<"min">> => 1.0, <<"max">> => 5.0}.

%% ---
%% int data
json_int(I) -> I.
%% int descriptor
dsc_int(0) -> #{<<"type">> => int};
dsc_int(1) -> #{<<"type">> => int, <<"min">> => 1, <<"max">> => 5}.

%% ---
%% str data
json_str(10) -> <<"abc">>;
json_str(20) -> <<"abc1">>;
json_str(30) -> <<"abc1;abc2;">>.
%% str descriptor
dsc_str(10) -> #{<<"type">> => str};
dsc_str(11) -> #{<<"type">> => str, <<"checkfun">> => [erlang,make_ref,[]]};
dsc_str(20) -> #{<<"type">> => str, <<"values">> => [<<"a">>,<<"abc1">>,<<"b">>]};
dsc_str(30) ->
    #{<<"type">> => str,
      <<"xsv">> => #{<<"separator">> => <<";">>,
                     <<"values">> => [<<"abc1">>,<<"abc2">>,<<"abc3">>]}
     }.

%% ---
%% object data
json_obj(10) ->
    #{<<"a">> => 1,
      <<"b">> => true,
      <<"c">> => 1.0,
      <<"d">> => [<<"1">>,<<"2">>,<<"3">>],
      <<"e">> => #{<<"e1">> => 1, <<"e2">> => #{<<"e21">> => [<<"211">>,<<"22">>]}},
      <<"f">> => [
                  #{<<"f1">> => <<"f1_value">>}
                 ],
      <<"x">> => <<"opt_str_x">>,
      <<"z">> => <<"opt_str_z">>,
      <<"g">> => #{<<"g1">> => true, <<"g2">> => [1,2,3]}
     }.
%% object descriptor
dsc_obj(10) ->
    #{<<"type">> => object,
      <<"object_rule">> =>
          #{<<"req">> =>
                #{<<"a">> => #{<<"type">> => int},
                  <<"b">> => #{<<"type">> => bool},
                  <<"c">> => #{<<"type">> => floatint},
                  <<"d">> => #{<<"type">> => strlist},
                  <<"e">> => #{<<"type">> => object,
                               <<"object_rule">> => #{<<"req">> =>
                                                          #{<<"e1">> =>  #{<<"type">> => int},
                                                            <<"e2">> =>  #{<<"type">> => object,
                                                                           <<"values">> => #{<<"type">> => strlist}
                                                                          }
                                                           },
                                                      <<"opt">> => #{<<"ex">> => int}
                                                     }
                              },
                  <<"f">> => #{<<"type">> => array,
                               <<"array_element">> => #{<<"type">> => object,
                                                        <<"object_rule">> =>
                                                            #{<<"req">> =>
                                                                  #{<<"f1">> =>  #{<<"type">> => str}},
                                                              <<"opt">> => #{<<"fx">> => int}
                                                             }
                                                       }
                              },
                  <<"g">> => #{<<"type">> => object,
                               <<"skip_unknown_keys">> => true,
                               <<"object_rule">> => #{<<"req">> => #{<<"g1">> =>  #{<<"type">> => bool}}}
                              }
                 },
            <<"opt">> =>
                #{<<"x">> => #{<<"type">> => str},
                  <<"z">> => #{<<"type">> => str}}
           }
     }.

-endif.