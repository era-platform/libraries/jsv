%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 15.10.2021
%%% @doc

-module(jsv_supv).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-behaviour(supervisor).

-export([init/1]).
-export([start_link/1]).
-export([start_child/1, restart_child/1, terminate_child/1, delete_child/1, get_childspec/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("jsv.hrl").

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").
-export([init_validation_types_ets/0, fill_validation_types_ets/0]).

-endif.

%% ====================================================================
%% API functions
%% ====================================================================

start_link(StartArgs) ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, StartArgs).

start_child(ChildSpec) ->
    supervisor:start_child(?MODULE, ChildSpec).

restart_child(Id) ->
    supervisor:restart_child(?MODULE, Id).

terminate_child(Id) ->
    supervisor:terminate_child(?MODULE, Id).

delete_child(Id) ->
    supervisor:delete_child(?MODULE, Id).

get_childspec(Id) ->
    supervisor:get_childspec(?MODULE, Id).

%% ====================================================================
%% Callback functions
%% ====================================================================

init(StartArgs) ->
    ?OUT('$trace',"Supv. inited with args:~120p", [StartArgs]),
    init_validation_types_ets(),
    fill_validation_types_ets(),
    SupFlags = #{strategy => one_for_one,
                 intensity => 10,
                 period => 2},
    {ok, {SupFlags, []}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
init_validation_types_ets() ->
    CreateEtsRes =
        case ets:info(?TypesEts,size) of
            undefined -> ets:new(?TypesEts, [named_table,public]);
            _ -> ok
        end,
    ?OUT('$trace',"Supv. init_validation_types_ets. result:~120p",[CreateEtsRes]),
    ok.

%% ---
fill_validation_types_ets() ->
    JsvEbinPath = get_jsv_ebin_path(),
    TypesBeams = filelib:wildcard(JsvEbinPath++"/jsv_validation_types_*.beam"),
    TypesModules = [?BU:to_atom_new(filename:basename(X,".beam")) || X <- TypesBeams],
    FGetTypeName = fun(TypeModule) ->
                           case catch TypeModule:type_name() of
                               {'EXIT',Reason} ->
                                   ?OUT('$trace',"Supv. fill_validation_types_ets. get type name for module (~120p) crash:~120p",[TypeModule,Reason]),
                                   false;
                               TypeName when is_binary(TypeName) -> {true,{TypeName,TypeModule}};
                               InvalidTypeName ->
                                   ?OUT('$trace',"Supv. fill_validation_types_ets. get invalid type name (~120p) for module (~120p) :~120p",[InvalidTypeName,TypeModule]),
                                   false
                           end
                   end,
    Types = lists:filtermap(FGetTypeName,TypesModules),
    ets:insert(?TypesEts,Types).

-ifdef(TEST).

%% @private
get_jsv_ebin_path() ->
    {ok,CWD} = file:get_cwd(),
    filename:join([CWD, "_build", "test", "lib", "jsv", "ebin"]).

-else.

%% @private
get_jsv_ebin_path() ->
    ?U:drop_last_subdir(code:which(?MODULE), 1).

-endif.